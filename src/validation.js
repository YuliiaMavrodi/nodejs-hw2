const Joi = require('@hapi/joi');

const registerValidation = (data) => {
  const schema = {
    username: Joi.string()
        .min(5)
        .required(),
    password: Joi.string()
        .min(6)
        .required(),
  };
  return Joi.validate(data, schema);
};

const loginValidation = (data) => {
  const schema = {
    username: Joi.string()
        .min(5)
        .required(),
    password: Joi.string()
        .min(6)
        .required(),
  };
  return Joi.validate(data, schema);
};

const createNoteValidation = (data) => {
  const schema = {
    text: Joi.string()
        .min(6)
        .required(),
  };
  return Joi.validate(data, schema);
};

const updateNoteValidation = (data) => {
  const schema = {
    text: Joi.string()
        .min(6),
  };
  return Joi.validate(data, schema);
};

module.exports.registerValidation = registerValidation;
module.exports.loginValidation = loginValidation;
module.exports.createNoteValidation = createNoteValidation;
module.exports.updateNoteValidation = updateNoteValidation;


