const express = require('express');
const app = express();
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const authRoute = require('./routes/auth');
const notesRoute = require('./routes/notes');
const usersRoute = require('./routes/users');
const verify = require('./verifyToken');
dotenv.config();

app.use(express.json());

app.use('/api/auth', authRoute);
app.use('/api/notes', verify, notesRoute);
app.use('/api/users/me', verify, usersRoute);


app.use((req, res, next) => {
  res.status(400).json({message: 'Not found'});
});

app.use((err, req, res, next) => {
  res.status(500).json({message: err.message});
});

const start = async () => {
  try {
    await mongoose.connect(
        process.env.DB_CONNECT, {
          useNewUrlParser: true,
          useUnifiedTopology: true,
        },
        () => console.log('connected to db!'));

    app.listen(8080, () => console.log('Server Up and running'));
  } catch (err) {
    console.error(err.message);
  }
};

start();


