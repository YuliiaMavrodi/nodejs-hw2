const express = require('express');
const router = new express.Router();
const User = require('../model/User');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const {registerValidation, loginValidation} = require('../validation');


router.post('/register', async (req, res) => {
  const {error} = registerValidation(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const usernameExist = await User.findOne({username: req.body.username});
  if (usernameExist) return res.status(400).send('Username already exists');

  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(req.body.password, salt);

  const user = new User({
    username: req.body.username,
    password: hashedPassword,
  });
  try {
    await user.save();
    res.json({message: 'Success'});
  } catch (e) {
    res.status(400).status(e);
  }
});

router.post('/login', async (req, res) => {
  const {error} = loginValidation(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const user = await User.findOne({username: req.body.username});
  if (!user) return res.status(400).send('Username in not found');

  const validPass = await bcrypt.compare(req.body.password, user.password);
  if (!validPass) return res.status(400).send('Invalid password');

  const token = jwt.sign({
    _id: user._id,
    username: user.username,
  }, process.env.TOKEN_SECRET);
  res.status(200).json({message: 'Success', jwt_token: token});
});

module.exports = router;
