const express = require('express');
const router = new express.Router();
const User = require('../model/User');
const bcrypt = require('bcryptjs');


router.get('/', async (req, res) => {
  try {
    const {userId} = req.user;
    const user = await User.findById(userId);
    res.status(200).send({
      user: {
        _id: user._id,
        username: user.username,
        createdDate: user.createdDate,
      },
    });
  } catch (error) {
    res.status(400).send({message: error.message});
  }
});

router.delete('/', async (req, res) => {
  try {
    const {userId} = req.user;
    await User.findByIdAndRemove(userId);
    res.status(200).send({message: 'Success'});
  } catch (error) {
    res.status(400).send({message: error.message});
  }
});

router.patch('/', async (req, res) => {
  try {
    const {userId} = req.user;
    const user = await User.findById(userId);
    const {oldPassword, newPassword} = req.body;

    const validPass = await bcrypt.compare(oldPassword, user.password);
    if (!validPass) return res.status(400).send('Invalid password');

    user.password = await bcrypt.hash(newPassword, 10);
    await user.save();
    res.status(200).send({message: 'Success'});
  } catch (error) {
    res.status(400).send({message: error.message});
  }
});
module.exports = router;
