const express = require('express');
const router = new express.Router();
const Note = require('../model/Note');
const {createNoteValidation, updateNoteValidation} = require('../validation');


router.post('/', async (req, res) => {
  const {error} = createNoteValidation(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  try {
    const {text} = req.body;
    const {userId} = req.user;

    if (!text) {
      return res.status(400).json({message: `Please specify 'text' parameter`});
    }

    const note = new Note({text, userId});
    await note.save();

    res.json({message: 'Success'});
  } catch (error) {
    res.status(400).json({message: error.message});
  }
});

router.get('/', async (req, res) => {
  try {
    const {userId} = req.user;

    const queryOffset = +req.query.offset || 0;
    const queryLimit = +req.query.limit || 20;
    const count = await Note.find({userId}).countDocuments();

    const notesList = await Note.find({userId}).select('-__v')
        .skip(queryOffset).limit(queryLimit);

    return res.status(200).send({
      offset: queryOffset,
      limit: queryLimit,
      count,
      notes: notesList,
    });
  } catch (error) {
    res.status(400).send({message: error.message});
  }
});

router.get('/:id', async (req, res) => {
  try {
    const noteId = req.params.id;
    const {userId} = req.user;

    const note = await Note.findOne({_id: noteId, userId}).select('-__v');
    if (!note) return res.status(400).send('No notes found');
    res.status(200).send({note});
  } catch (error) {
    res.status(400).send({message: error.message});
  }
});

router.put('/:id', async (req, res) => {
  const {error} = updateNoteValidation(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  try {
    const noteId = req.params.id;
    const {userId} = req.user;
    const newNoteText = req.body.text;

    await Note.findOneAndUpdate({_id: noteId, userId}, {text: newNoteText});

    res.status(200).send({message: 'Success'});
  } catch (error) {
    res.status(400).send({message: error.message});
  }
});

router.patch('/:id', async (req, res) => {
  try {
    const noteId = req.params.id;
    const {userId} = req.user;

    const note = await Note.findOne({_id: noteId, userId});
    note.completed = !note.completed;

    note.save();

    res.status(200).send({message: 'Success'});
  } catch (error) {
    res.status(400).send({message: error.message});
  }
});

router.delete('/:id', async (req, res) => {
  try {
    const noteId = req.params.id;

    await Note.findByIdAndRemove(noteId);

    res.status(200).send({message: 'Success'});
  } catch (error) {
    res.status(400).send({message: error.message});
  }
});

module.exports = router;
