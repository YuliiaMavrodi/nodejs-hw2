const jwt = require('jsonwebtoken');

module.exports = function(req, res, next) {
  const {
    authorization,
  } = req.headers;

  if (!authorization) {
    return res
        .status(400)
        .json({message: 'Please, provide "authorization" header'});
  }
  const token = authorization.split(' ')[1];
  if (!token) {
    return res.status(400).json({message: 'Please, include token to request'});
  }
  try {
    const verified = jwt.verify(token, process.env.TOKEN_SECRET);
    req.user = {
      userId: verified._id,
      username: verified.username,
    };
    next();
  } catch (e) {
    res.status(400).send('Invalid token');
  }
};
